//
//  NetworkingClient.swift
//  NavigationControllerTask
//
//  Created by alex on 14.07.21.
//

import Foundation
import Alamofire

class NetworkingClient {

    typealias WebServiceResponse = ([[String: Any]]?, Error?) -> Void
    
    func executeRequest(withURL url: URL, completion: @escaping WebServiceResponse) {
        AF.request(url).validate().responseJSON { response in
            if let error = response.error {
                completion(nil, error)
            } else if let jsonArray = response.value as? [[String: Any]] {
                completion(jsonArray, nil)
            }
        }
    }
    
}
