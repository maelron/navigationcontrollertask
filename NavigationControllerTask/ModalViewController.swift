//
//  ModalViewController.swift
//  NavigationControllerTask
//
//  Created by alex on 13.07.21.
//

import UIKit

class ModalViewController: UIViewController, UITextFieldDelegate {

    let dismissButton = UIButton()
    let presentButton = UIButton()
    let modalVCCounterLabel = UILabel()
    let numberTextField = UITextField()
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        print("ModalVC viewDidLoad")
        
        view.backgroundColor = .systemGray
        
        modalVCCounterLabel.text = "\(counter)"
        
        numberTextField.layer.borderWidth = 1.5;
        numberTextField.layer.cornerRadius = 5.0;
        numberTextField.layer.borderColor = UIColor.label.cgColor;
        numberTextField.clipsToBounds = true
        numberTextField.textAlignment = .center
        numberTextField.keyboardType = .numberPad
        numberTextField.delegate = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGestureRecognizer)
        
        dismissButton.setTitle("Dismiss", for: .normal)
        dismissButton.addTarget(self, action: #selector(dismissButtonClicked), for: .touchUpInside)

        presentButton.setTitle("Present", for: .normal)
        presentButton.addTarget(self, action: #selector(presentButtonClicked), for: .touchUpInside)
        
        setPropertiesConstraints()
    }
    
    func setPropertiesConstraints() {
        dismissButton.translatesAutoresizingMaskIntoConstraints = false
        presentButton.translatesAutoresizingMaskIntoConstraints = false
        modalVCCounterLabel.translatesAutoresizingMaskIntoConstraints = false
        numberTextField.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(dismissButton)
        view.addSubview(presentButton)
        view.addSubview(modalVCCounterLabel)
        view.addSubview(numberTextField)
        
        dismissButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20.0).isActive = true
        dismissButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -100.0).isActive = true
        
        presentButton.bottomAnchor.constraint(equalTo: dismissButton.bottomAnchor).isActive = true
        presentButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 100).isActive = true
        
        modalVCCounterLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        modalVCCounterLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        numberTextField.topAnchor.constraint(equalTo: modalVCCounterLabel.bottomAnchor, constant: 10.0).isActive = true
        numberTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 150).isActive = true
        numberTextField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -150.0).isActive = true
    }
    
    @objc func presentButtonClicked() {
        let newModalVC = ModalViewController()
        newModalVC.counter = counter + 1
        present(newModalVC, animated: true, completion: nil)
    }
    
    func checkCounter(counter: Int) {
        if let presentingVC = self.presentingViewController as? ModalViewController {
            if presentingVC.counter != counter {
                presentingVC.checkCounter(counter: counter)
            } else {
                presentingVC.dismiss(animated: true, completion: nil)
            }
        } else {
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func dismissButtonClicked() {
        if numberTextField.text != "" {
            if let textFieldText = numberTextField.text {
                if let intValueOfTextFieldText = Int(textFieldText) {
                    checkCounter(counter: intValueOfTextFieldText)
                }
            }
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /*
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("ModalVC View Will Appear is called")

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("ModalVC View Did Appear is called")

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("ModalVC View Will Dissappear is called")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("ModalVC View Did Dissappear is called")
    }
    */
    
}
