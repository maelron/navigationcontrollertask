//
//  RoundedButton.swift
//  NavigationControllerTask
//
//  Created by alex on 16.07.21.
//

import UIKit

class RoundedButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = frame.size.height / 2.0
        
        if self.isHighlighted {
            layer.borderColor = UIColor.separator.cgColor
        } else {
            layer.borderColor = UIColor.label.cgColor
        }
        
    }
    
    func customizeButton() {
        layer.borderWidth = 1.5
        setImage(UIImage.init(named: "sync"), for: .normal)
        setImage(UIImage.init(named: "syncBlue"), for: .highlighted)
        imageEdgeInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        imageView?.contentMode = .scaleAspectFit
        clipsToBounds = true
    }
    
}
