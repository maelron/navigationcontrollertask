//
//  RGBViewController.swift
//  NavigationControllerTask
//
//  Created by alex on 17.07.21.
//

import UIKit

class RGBViewController: UIViewController {
    
    enum ButtonType: String {
        case push
        case pop
        case popToRoot
    }
    
    var pushButtonType: ButtonType
    let pushButton = UIButton()
    
    var viewColor: UIColor
    
    
    init(backgroundColor: UIColor, buttonType: ButtonType) {
        viewColor = backgroundColor
        pushButtonType = buttonType
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillLayoutSubviews() {
        view.layer.cornerRadius = view.bounds.height / 2.0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = viewColor
        
        setupPushButton()
    }
    
    func setupPushButton() {
        
        pushButton.setTitle(pushButtonType.rawValue, for: .normal)
        pushButton.setTitleColor(.label, for: .normal)
        pushButton.setTitleColor(.systemTeal, for: .highlighted)
        
        pushButton.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(pushButton)
        
        pushButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pushButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        pushButton.addTarget(self, action: #selector(pushButtonClicked), for: .touchUpInside)
    }

    @objc func pushButtonClicked() {
        switch pushButtonType {
        case .push:
            navigationController?.pushViewController(ContainerViewController(), animated: true)
        case .pop:
            navigationController?.popViewController(animated: true)
        case .popToRoot:
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    /*
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("\(pushButtonType.rawValue) View Controller's view Will Appear is called")

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("\(pushButtonType.rawValue) View Controller's view Did Appear is called")

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("\(pushButtonType.rawValue) View Controller's view Will Dissappear is called")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("\(pushButtonType.rawValue) View Did Dissappear is called")
    }
    */
    
    
    
}
