//
//  UserlistViewController.swift
//  NavigationControllerTask
//
//  Created by alex on 14.07.21.
//

import UIKit
import CoreData

class UserlistViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {

    var fetchedResultsController: NSFetchedResultsController<NSManagedObject>!
    
//    var usersArray = [[String : Any]]()
    let networkClient = NetworkingClient()
    let reloadButton = RoundedButton()
    let tableView = UITableView()
        
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .secondarySystemBackground
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNewPerson))
        
        setupSubviews()
        
        initializeFetchedResultsController()
    }
    
    func getManagedContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        
        return managedContext
    }
            
    @objc func addNewPerson() {
        let addNewPersonAlertController = UIAlertController(title: "Add new Person", message: nil, preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
            if let name = addNewPersonAlertController.textFields![0].text {
                if let age = addNewPersonAlertController.textFields![1].text {
                    if name.isEmpty {
                        let alert = UIAlertController(title: "Name field can't be empty!", message: "Please enter a valid name", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        self.savePerson(personName: name, personAge: UInt16(age) ?? 0)
                    }
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        addNewPersonAlertController.addAction(cancelAction)
        addNewPersonAlertController.addAction(saveAction)
        
        addNewPersonAlertController.addTextField(configurationHandler: nil)
        addNewPersonAlertController.addTextField(configurationHandler: nil)
        
        addNewPersonAlertController.textFields![0].placeholder = "Enter name"
        addNewPersonAlertController.textFields![1].placeholder = "Enter age"
        addNewPersonAlertController.textFields![1].keyboardType = .numberPad
        
        present(addNewPersonAlertController, animated: true, completion: nil)
    }
    
    @objc func executeRequestFromDropBox() {
        guard let urlToExecute = URL(string: "https://www.dropbox.com/s/c8941tq37m8tg7j/db.json?dl=1") else { return }
        
        networkClient.executeRequest(withURL: urlToExecute) { [self] jsonArray, error in
            if let error = error {
                print(error.localizedDescription)
            } else if let jsonArray = jsonArray {
                for user in jsonArray {
                    if let name = user["name"] as? String, let age = user["age"] as? UInt16 {
                        self.savePerson(personName: name, personAge: age)
                    }
                }
            }
        }
    }
    
//    MARK: Visual setup methods
    
    func setupButton() {
        reloadButton.customizeButton()
        reloadButton.addTarget(self, action: #selector(executeRequestFromDropBox), for: .touchUpInside)
        view.addSubview(reloadButton)
    }
    
    func setupTableView() {
        tableView.backgroundColor = .secondarySystemBackground
        tableView.rowHeight = 80
        tableView.register(SubtitleTableViewCell.self, forCellReuseIdentifier: "Cell")
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
    }
 
    func setupSubviews() {
        setupButton()
        setupTableView()

        reloadButton.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            reloadButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -5.0),
            reloadButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            reloadButton.widthAnchor.constraint(equalToConstant: 50.0),
            reloadButton.heightAnchor.constraint(equalToConstant: 50.0),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: reloadButton.topAnchor, constant: -5.0)
        
        ])
    }

    
//    MARK: Core Data methods
    
    func initializeFetchedResultsController() {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Person")
        let nameSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [nameSortDescriptor]
        fetchRequest.fetchBatchSize = 6
        
        let managedContext = getManagedContext()
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: "name", cacheName: nil)
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
        
    }
    
    func savePerson(personName name: String, personAge age: UInt16) {
        let managedContext = getManagedContext()
        
        let entity = NSEntityDescription.entity(forEntityName: "Person", in: managedContext)!
        
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        
        person.setValue(name, forKeyPath: "name")
        person.setValue(age, forKeyPath: "age")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo) ")
        }
    }
    
//    MARK: NSFetchedResultsControllerDelegate methods
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String? {
        return sectionName
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        @unknown default:
            fatalError("No such NSFetchedResultsChangeType handled!")
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    
//    MARK: Table view data source methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = fetchedResultsController.sections else { return 0 }
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let sections = fetchedResultsController.sections else { return nil }
        
        return sections[section].indexTitle ?? ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else { return 0 }
        return sections[section].numberOfObjects
        
//        without sections
//        guard let count = fetchedResultsController.fetchedObjects?.count  else { return 0 }
//        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
       
        cell.backgroundColor = .secondarySystemBackground
        
        let person = fetchedResultsController.object(at: indexPath)

        cell.textLabel?.text = person.value(forKeyPath: "name") as? String
        if let age = person.value(forKeyPath: "age") {
            cell.detailTextLabel?.text = "\(age)"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let managedContext = getManagedContext()

            let person = fetchedResultsController.object(at: indexPath)
            managedContext.delete(person)
            
            do {
                try managedContext.save()
            } catch {
                fatalError("Failed to delete object!")
            }
        }
    }

}
