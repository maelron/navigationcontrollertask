//
//  ContainerViewController.swift
//  NavigationControllerTask
//
//  Created by alex on 13.07.21.
//

import UIKit

class ContainerViewController: UIViewController {

    let stackView = UIStackView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        print("ContainerVC viewDidLoad")
        
        view.backgroundColor = .secondarySystemBackground
        
        configureStackView()
    }
    
    func configureStackView() {

        let redChild = RGBViewController(backgroundColor: .systemRed, buttonType: .push)
        let blueChild = RGBViewController(backgroundColor: .systemBlue, buttonType: .pop)
        let greenChild = RGBViewController(backgroundColor: .systemGreen, buttonType: .popToRoot)
        
        addChild(redChild)
        addChild(blueChild)
        addChild(greenChild)
        
        stackView.addArrangedSubview(redChild.view)
        stackView.addArrangedSubview(blueChild.view)
        stackView.addArrangedSubview(greenChild.view)

        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 8.0
        
        view.addSubview(stackView)
        
        redChild.didMove(toParent: self)
        blueChild.didMove(toParent: self)
        greenChild.didMove(toParent: self)
        
        setStackViewConstraints()
    }
    
    func setStackViewConstraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20.0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20.0).isActive = true
        stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8.0).isActive = true
        stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8.0).isActive = true
    }
}
